import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './Home/home.component';
import { SignUpComponent } from './SignUp/Signup.component';

const appRoutes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
      path: "signup",
      component:SignUpComponent
  },

  { path: "**", redirectTo: "" }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
