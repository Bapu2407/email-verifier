const Note = require('../models/note.model.js');
let Promise = require('bluebird'),
    verify = Promise.promisify(require('./index.js').verify),
    argv = process.argv.slice(2),
    loggerOptions = require('./logger.js').loggerOptions,
    logger = require('./logger.js').logger

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Create a Note
    const note = new Note({
        title: req.body.title || "Untitled Note",
        content: req.body.content
    });

    // Save Note in the database
    note.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });

};
const infoCodes = {
    finishedVerification: 1,
    invalidEmailStructure: 2,
    noMxRecords: 3,
    SMTPConnectionTimeout: 4,
    domainNotFound: 5,
    SMTPConnectionError: 6
}
// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Note.find()
        .then(notes => {
            res.send(notes);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    //send( req.params.noteId + "Some error occurred while retrieving notes.")
    let email = req.params.noteId;
     verify(email)
      .then((info) => {
          console.log(info);
          return res.send(info);
         // return (info)
        
      })
      .catch((err) => {
        console.log(err);
       if(err.code == "ENOTFOUND") 
       {
        err.cause.success = 'false';
        return res.send(err.cause);
       }
      })
    module.exports.verify = function verify(email,options,callback){
        console.log(email);
        let params = {}
        let args = (arguments.length === 1 ? [arguments[0]] : Array.apply(null, arguments))
      
        args.forEach(function(arg){
          if( typeof arg === 'string' ){
            params.email = arg
          }
          else if( typeof arg === 'object' ){
            params.options = arg
          }
          else if( typeof arg === 'function' ){
            params.callback = arg
          }
        })
      
        if( !params.email && params.options.email && typeof params.options.email === 'string' ) params.email = params.options.email
        params.options = optionsDefaults(params.options)
      
        if( !params.email ) throw new Error(errors.missing.email)
        if( !params.options ) throw new Error(errors.missing.options)
        if( !params.callback ) throw new Error(errors.missing.callback)
      
        if( !validator(params.email) ) return params.callback(null, { success: false, info: 'Invalid Email Structure', addr: email, params: params, code: infoCodes.invalidEmailStructure })
      
        if( params.options.dns ) dnsConfig(params.options)
      
        logger.info("# Veryfing " + params.email)
      
        startDNSQueries(params)
      
      }
      module.exports.verifyCodes = infoCodes;
      function startDNSQueries(params){
        console.log(email);
      
        let domain = params.email.split(/[@]/).splice(-1)[0].toLowerCase()
      
        logger.info("Resolving DNS... " + domain)
        dns.resolveMx(domain,(err,addresses) => {
          if (err || (typeof addresses === 'undefined')) {
            params.callback(err, { success: false, info: 'Domain not found', code: infoCodes.domainNotFound });
          }
          else if (addresses && addresses.length <= 0) {
            params.callback(null, { success: false, info: 'No MX Records', code: infoCodes.noMxRecords });
          }
          else{
            params.addresses = addresses
      
            // Find the lowest priority mail server
            let priority = 10000,
                lowestPriorityIndex = 0
      
            for (let i = 0 ; i < addresses.length ; i++) {
              if (addresses[i].priority < priority) {
                  priority = addresses[i].priority
                  lowestPriorityIndex = i
                  logger.info('MX Records ' + JSON.stringify(addresses[i]))
              }
            }
      
            params.options.smtp = addresses[lowestPriorityIndex].exchange
            logger.info("Choosing " + params.options.smtp + " for connection")
            beginSMTPQueries(params)
          }
      
      
        })
      }
      
      function beginSMTPQueries(params){
        console.log(email);
      
      
        let stage = 0,
            success = false,
            response = '',
            completed = false,
            ended = false,
            tryagain = false,
            banner = ''
      
        logger.info("Creating connection...")
        let socket = net.createConnection(params.options.port, params.options.smtp)
      
        let callback = (err,object) => {
          callback = () => {} // multiple sources could call the callback, replace the function immediately to prevent it from being called twice
          ended = true
          console.log('hi');
          return params.callback(err,object)
        }
      
        let advanceToNextStage = () => {
          stage++
          response = ''
        }
      
        if( params.options.timeout > 0 ){
          socket.setTimeout(params.options.timeout,() => {
            callback(null,{ success: false, info: 'Connection Timed Out', addr: params.email, code: infoCodes.SMTPConnectionTimeout, tryagain:tryagain })
            socket.destroy()
          })
        }
      
        socket.on('data', function(data) {
          response += data.toString();
          completed = response.slice(-1) === '\n';
          if (completed) {
              logger.server(response)
              switch(stage) {
                  case 0: if (response.indexOf('220') > -1 && !ended) {
                              // Connection Worked
                              banner = response
                              var cmd = 'EHLO '+params.options.fqdn+'\r\n'
                              logger.client(cmd)
                              socket.write(cmd,function() { stage++; response = ''; });
                          }
                          else{
                              if (response.indexOf('421') > -1 || response.indexOf('450') > -1 || response.indexOf('451') > -1)
                                  tryagain = true;
                              socket.end();
                          }
                          break;
                  case 1: if (response.indexOf('250') > -1 && !ended) {
                              // Connection Worked
                              var cmd = 'MAIL FROM:<'+params.options.sender+'>\r\n'
                              logger.client(cmd)
                              socket.write(cmd,function() { stage++; response = ''; });
                          }
                          else{
                              socket.end();
                          }
                          break;
                  case 2: if (response.indexOf('250') > -1 && !ended) {
                              // MAIL Worked
                              var cmd = 'RCPT TO:<' + params.email + '>\r\n'
                              logger.client(cmd)
                              socket.write(cmd,function() { stage++; response = ''; });
                          }
                          else{
                              socket.end();
                          }
                          break;
                  case 3: if (response.indexOf('250') > -1 || (params.options.ignore && response.indexOf(params.options.ignore) > -1)) {
                              // RCPT Worked
                              success = true;
                          }
                          stage++;
                          response = '';
                          // close the connection cleanly.
                          if(!ended) {
                            var cmd = 'QUIT\r\n'
                            logger.client(cmd)
                            socket.write(cmd);
                          }
                          break;
                  case 4:
                    socket.end();
              }
      
          }
        })
      
        socket.once('connect', function(data) {
          logger.info("Connected")
        })
      
        socket.once('error', function(err) {
          logger.error("Connection error")
          callback( err, { success: false, info: 'SMTP connection error', addr: params.email, code: infoCodes.SMTPConnectionError, tryagain:tryagain })
        })
      
        socket.once('end', function() {
          logger.info("Closing connection")
          console.log("hi");
          callback(null, { success: success, info: (params.email + ' is ' + (success ? 'a valid' : 'an invalid') + ' address'), addr: params.email, code: infoCodes.finishedVerification, tryagain:tryagain, banner:banner })
        })
      }
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.content) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Note.findByIdAndUpdate(req.params.noteId, {
        title: req.body.title || "Untitled Note",
        content: req.body.content
    }, { new: true })
        .then(note => {
            if (!note) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send(note);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            return res.status(500).send({
                message: "Error updating note with id " + req.params.noteId
            });
        });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
        .then(note => {
            if (!note) {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            res.send({ message: "Note deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Note not found with id " + req.params.noteId
                });
            }
            return res.status(500).send({
                message: "Could not delete note with id " + req.params.noteId
            });
        });
};
